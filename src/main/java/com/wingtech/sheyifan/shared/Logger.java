package com.wingtech.sheyifan.shared;

import com.wingtech.sheyifan.Main;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Logger {
    public static void printToolLog(String log) {
        var logDes = Paths.get(Main.logPath.getText()).resolve("logs").resolve("tool").resolve("fxlogcat.txt");
        try {
            if (Files.notExists(logDes.getParent())) {
                Files.createDirectories(logDes.getParent());
            }
            if (Files.notExists(logDes)) {
                Files.createFile(logDes);
            }
        }catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try(var writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logDes.toFile(), true)))) {
            writer.write(log);
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void printToolError(String log) {
        var logDes = Paths.get(Main.logPath.getText()).resolve("logs").resolve("tool").resolve("fxlogcat.txt");
        try {
            if (Files.notExists(logDes.getParent())) {
                Files.createDirectories(logDes.getParent());
            }
            if (Files.notExists(logDes)) {
                Files.createFile(logDes);
            }
        }catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try(var writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logDes.toFile(), true)))) {
            writer.write("Error: " + log);
            writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
