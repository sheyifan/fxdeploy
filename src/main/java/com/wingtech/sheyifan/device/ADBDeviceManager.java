package com.wingtech.sheyifan.device;

import com.wingtech.sheyifan.shared.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;

public class ADBDeviceManager implements DeviceManager {
    volatile boolean reading = true;

    @Override
    public boolean testConnection() throws IOException, InterruptedException {
        var proc = Runtime.getRuntime().exec("adb devices");
        // 2021-03-20 sheyifan Issue: line count >=3 : connected
        int[] lineCount = new int[1];

        var consoleListener = new Thread(() -> {
            var reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String buf = null;

            while (true) {
                if(!reading) {
                    break;
                }
                try {
                    if ((buf = reader.readLine()) != null) {
                        lineCount[0]++;
                        System.out.println(buf);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        consoleListener.start();

        proc.waitFor();
        reading = false;

        return lineCount[0] >= 3;
    }

    @Override
    public void exportLog(LogOptions logOptions) {
        var toolDes = logOptions.des.resolve("logs").resolve("tool");
        if (Files.notExists(toolDes)) {
            try {
                Files.createDirectories(toolDes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (logOptions.ifconfig) {
            long startTime = System.currentTimeMillis();
            logOptions.exportIpConfig();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting ifconfig log, time: " + (endTime - startTime) + "ms");
        }

        if (logOptions.iptable) {
            long startTime = System.currentTimeMillis();
            logOptions.exportIptable();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting iptable log, time: " + (endTime - startTime) + "ms");
        }

        if(logOptions.iproute) {
            long startTime = System.currentTimeMillis();
            logOptions.exportIproute();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting iproute log, time: " + (endTime - startTime) + "ms");
        }

        if (logOptions.dns) {
            long startTime = System.currentTimeMillis();
            logOptions.exportDNSInfo();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting DNS information, time: " + (endTime - startTime) + "ms");
        }

        if(logOptions.sim) {
            long startTime = System.currentTimeMillis();
            logOptions.exportSIMStatus();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting SIM information, time: " + (endTime - startTime) + "ms");
        }

        if (logOptions.qcmap) {
            long startTime = System.currentTimeMillis();
            logOptions.exportQCMAPPID();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting QCMAP_Web_CLIENT process id, time: " + (endTime - startTime) + "ms");
        }

        if(logOptions.ping) {
            long startTime = System.currentTimeMillis();
            logOptions.exportPingLog();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting ping log, time: " + (endTime - startTime) + "ms");
        }

        if(logOptions.systemLog) {
            long startTime = System.currentTimeMillis();
            logOptions.exportSystemLog();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting system log, time: " + (endTime - startTime) + "ms");
        }

        if(logOptions.boot) {
            long startTime = System.currentTimeMillis();
            logOptions.exportBootLog();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting boot log, time: " + (endTime - startTime) + "ms");
        }

        if(logOptions.version) {
            long startTime = System.currentTimeMillis();
            logOptions.exportVersionLog();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting version information, time: " + (endTime - startTime) + "ms");
        }

        if(logOptions.adbLog) {
            long startTime = System.currentTimeMillis();
            logOptions.exportAdbLog();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting /data/logs, time: " + (endTime - startTime) + "ms");
        }

        if (logOptions.tcpdump) {
            long startTime = System.currentTimeMillis();
            logOptions.exportTcpDump();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting tcpdump with 1 package, time: " + (endTime - startTime) + "ms");
        }

        if (logOptions.tmp) {
            long startTime = System.currentTimeMillis();
            logOptions.exportTmpLog();
            long endTime = System.currentTimeMillis();
            Logger.printToolLog("Finish exporting /tmp, time: " + (endTime - startTime) + "ms");
        }
    }
}
