package com.wingtech.sheyifan.device;

import java.io.IOException;

public interface DeviceManager {
    boolean testConnection() throws IOException, InterruptedException;
    void exportLog(LogOptions logOptions);
}
