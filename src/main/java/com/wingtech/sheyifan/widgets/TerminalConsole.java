package com.wingtech.sheyifan.widgets;

import com.jfoenix.controls.JFXTextArea;

import java.io.OutputStream;

public class TerminalConsole extends OutputStream {
    private JFXTextArea consoleWindow;

    @Override
    public void write(int b) {
        consoleWindow.appendText(String.valueOf((char) b));
    }

    public TerminalConsole(JFXTextArea consoleWindow) {
        this.consoleWindow = consoleWindow;
    }
}
