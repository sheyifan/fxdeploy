package com.wingtech.sheyifan.front;

import com.jfoenix.controls.JFXButton;
import com.wingtech.sheyifan.shared.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

/**
 * 2020-12-12 20:52 sheyifan Issue: manage layout in the front
 */
public class Layer {
    private StackPane root;

    public Layer(StackPane root) {
        this.root = root;
    }

    // 2020-12-12 20:54 sheyifan Issue: manage all layout
    public void manageAll() {
        layNavigateBar();
    }

    // 2020-12-13 13:07 sheyifan Issue: layout navigate bar and body on the right
    // 2020-12-14 00:43 sheyifan Issue: bind pref<Scale>Property to <scale>Property
    public void layNavigateBar() {
        var navigateBar = (Pane) root.lookup("#nav-bar");
        var appBody = (StackPane) root.lookup("#app-body");
        if (navigateBar == null | appBody == null) {
            Logger.printToolError("Error: unable to find navigate-bar from FXML.");
            return;
        }
        var appIconPane = (Pane) navigateBar.lookup("#app-icon-pane");
        var appIcon = (Label) navigateBar.lookup("#app-icon");
        var navSeparator = (Separator) navigateBar.lookup("#nav-bar-sep");

        // Layout navigate bar
        navigateBar.prefHeightProperty().bind(root.heightProperty());
        appIconPane.prefWidthProperty().bind(navigateBar.widthProperty());
        // 2020-12-14 06:38 sheyifan Issue: bidirectional binding, ensure width = height
        // appIconPane.prefHeightProperty().bindBidirectional(appIconPane.prefWidthProperty());
        appIconPane.prefHeightProperty().bind(appIconPane.prefWidthProperty().divide(2));
        navSeparator.prefWidthProperty().bind(navigateBar.prefWidthProperty().subtract(12));
        appIcon.setEffect(new InnerShadow());
        // Manually bind properties which is not built-in
        navigateBar.widthProperty().addListener((observable, oldWidth, newWidth) -> {

        });

        // Layout body
        appBody.layoutXProperty().bind(navigateBar.widthProperty());
        appBody.prefWidthProperty().bind(root.widthProperty().subtract(navigateBar.widthProperty()));
        appBody.prefHeightProperty().bind(root.heightProperty());

        // 2020-12-14 00:51 sheyifan Issue: select children to top (the last node) in StackPane, which
        // modify z-index
        appBody.lookup("#main-page").toFront();
    }
}
