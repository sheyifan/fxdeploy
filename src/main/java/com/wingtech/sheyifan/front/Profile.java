package com.wingtech.sheyifan.front;

import com.jfoenix.controls.JFXTextField;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

/**
 * 2020-12-12 22:07 sheyifan Issue: set profile settings and values in the front
 */
public class Profile {
    private StackPane root;

    public Profile(StackPane root) {
        this.root = root;
    }

    public void setAll() {
        setNavStyle();
        initLogPath();
        root.lookup("#export-progress").setOpacity(0);
    }

    public void setNavStyle() {
        var navBar = (Pane) root.lookup("#nav-bar");
        var navTitle = (Label) root.lookup("#nav-title");

        navTitle.setEffect(new InnerShadow());
    }

    // 2021-03-20 sheyifan Issue: fill initial path of logs
    public void initLogPath() {
        var logPath = (JFXTextField) root.lookup("#log-location");
        if (logPath != null) {
            logPath.setText(System.getProperty("user.dir"));
        }
    }
}
