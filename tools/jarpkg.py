import datetime
import subprocess
import os
from pathlib import Path

root = Path(__file__).parent.absolute()

# Use jar command to generate class packages
def jarpkg(entrance: str, classPath: str, classes: str):
    # Create new path for minimum jre by current date
    jarFullPath = root.__str__() + '/jars' + '/' + \
        "jfxpkg" + '-' + \
        str(datetime.datetime.now().year) + '-' + \
        str(datetime.datetime.now().month) + "-" + \
        str(datetime.datetime.now().day) + "_" + \
        str(datetime.datetime.now().hour) + "-" + \
        str(datetime.datetime.now().second) + "-" + \
        str(datetime.datetime.now().microsecond) + ".jar"

    if not os.path.exists(Path(jarFullPath).parent):
        os.makedirs(Path(jarFullPath).parent)

    # Python CLI
    cmd = ["jar", 
        "cvfe", 
        jarFullPath,
        entrance,
        "-C", classPath,
        classes]

    print(f"Execute command:")
    for param in cmd:
        print(param, end=" ")
    print()
    output = subprocess.run(cmd, capture_output=True)
    # Output log seperately. Decoded by UTF-8
    print("stdout:" + output.stdout.decode("utf-8"))
    print("stderr:" + output.stderr.decode("utf-8"))

if __name__ == '__main__':
    print("Warning: this program can only package my own source code, not for third-party.\n" + 
    "If uses third-party in source code, maven/gradle is the best choice.")

    classPath: str = os.path.join(root.parent.absolute(), "target", "classes")
    jarpkg("com.wingtech.sheyifan.Main", classPath, "com")